#!/bin/sh

# Slackware build script for geoclue2

# Copyright 2015  Widya Walesa <walecha99@gmail.com>
# All rights reserved.
#
# Redistribution and use of this script, with or without modification, is
# permitted provided that the following conditions are met:
#
# 1. Redistributions of this script must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
#
# THIS SOFTWARE IS PROVIDED BY THE AUTHOR ''AS IS'' AND ANY EXPRESS OR IMPLIED
# WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
# MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO
# EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
# SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
# PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
# OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
# WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
# OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
# ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. FURTHERMORE I AM NOT LIABLE IF
# YOUR DATA IS DESTROYED, YOUR HOUSE BURNS DOWN OR YOUR DOG RUNS OFF.

SRCNAM=geoclue
PRGNAM=geoclue2
VERSION=${VERSION:-2.4.12}
BUILD=${BUILD:-3}
TAG=${TAG:-_wlsgnome}

NUMJOBS=${NUMJOBS:-"-j$(echo "$(nproc) * 7 / 8" | bc)"}

if [ -z "$ARCH" ]; then
  case "$( uname -m )" in
    i?86) ARCH=i586 ;;
    arm*) ARCH=arm ;;
       *) ARCH=$( uname -m ) ;;
  esac
fi

CWD=$(pwd)
WRK=${WRK:-/tmp/wlsbuild}
PKG=$WRK/package-$PRGNAM
OUTPUT=${OUTPUT:-/tmp}
DOCS="COPYING NEWS README"

if [ "$ARCH" = "i586" ]; then
  SLKCFLAGS="-O2 -pipe -march=i586 -mtune=i686"
  LIBDIRSUFFIX=""
elif [ "$ARCH" = "i686" ]; then
  SLKCFLAGS="-O2 -pipe -march=i686 -mtune=i686"
  LIBDIRSUFFIX=""
elif [ "$ARCH" = "x86_64" ]; then
  SLKCFLAGS="-O2 -m64 -pipe -fPIC"
  LIBDIRSUFFIX="64"
else
  SLKCFLAGS="-O2"
  LIBDIRSUFFIX=""
fi

set -e

unset DOWNLOAD
eval `cat $PRGNAM.info | grep "DOWNLOAD="`
if [ ! -f `basename $DOWNLOAD` ]; then
  echo "Downloading: $DOWNLOAD"
  rm -f $PRGNAM-$VERSION.tar.*
  curl -OLJ -C - $DOWNLOAD
fi

rm -rf $PKG
mkdir -p $WRK $PKG $OUTPUT
cd $WRK
rm -rf $SRCNAM-$VERSION
tar xvf $CWD/$SRCNAM-$VERSION.tar.?z*
cd $SRCNAM-$VERSION

chown -R root:root .
find . \
 \( -perm 777 -o -perm 775 -o -perm 711 -o -perm 555 -o -perm 511 \) \
 -exec chmod 755 {} \; -o \
 \( -perm 666 -o -perm 664 -o -perm 600 -o -perm 444 -o -perm 440 -o -perm 400 \) \
 -exec chmod 644 {} \;

export CFLAGS="$SLKCFLAGS -flto=auto -ffat-lto-objects "
export CXXFLAGS="$SLKCFLAGS -flto=auto -ffat-lto-objects "
export OBJCFLAGS="$SLKCFLAGS -flto=auto -ffat-lto-objects "
export LDFLAGS="$LDFLAGS -flto=auto "
./configure \
  --build=$ARCH-slackware-linux \
  --prefix=/usr \
  --libdir=/usr/lib$LIBDIRSUFFIX \
  --mandir=/usr/man \
  --infodir=/usr/info \
  --sysconfdir=/etc \
  --docdir=/usr/doc/$PRGNAM-$VERSION \
  --localstatedir=/var \
  --disable-static \
  --enable-shared \
  || exit 1

make $NUMJOBS || make -j1 V=1 || exit 1
make install DESTDIR=$PKG || exit 1

mv $PKG/etc/geoclue/geoclue.conf $PKG/etc/geoclue/geoclue.conf.new

# Keep /etc/dbus-1/system.d for user overrides rules
cp -r $PKG/etc/dbus-1 $PKG/usr/share/
rm -r $PKG/etc/dbus-1

find $PKG | xargs file | grep -e "executable" -e "shared object" | grep ELF \
  | cut -f 1 -d : | xargs strip --strip-unneeded 2> /dev/null || true

mkdir -p $PKG/usr/doc/$PRGNAM-$VERSION
cp -a $DOCS $PKG/usr/doc/$PRGNAM-$VERSION
cat $CWD/$PRGNAM.SlackBuild > $PKG/usr/doc/$PRGNAM-$VERSION/$PRGNAM.SlackBuild

# Don't ship .la files
rm -f ${PKG}/{,usr/}lib{,64}/*.la

mkdir -p $PKG/install
cat $CWD/doinst.sh > $PKG/install/doinst.sh
cat $CWD/slack-desc > $PKG/install/slack-desc

cd $PKG
/sbin/makepkg -p -l y -c n $OUTPUT/$PRGNAM-$VERSION-$ARCH-$BUILD$TAG.${PKGTYPE:-"tlz"}

echo "Cleaning up build directory"
cd $WRK; rm -rf $SRCNAM-$VERSION $PKG
