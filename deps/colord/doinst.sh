if ! getent group | grep -q '^colord:'; then
  /usr/sbin/groupadd -g 303 colord
fi

if ! getent passwd | grep -q '^colord:'; then
  /usr/sbin/useradd -d /var/lib/colord -s /bin/false -g colord -u 303 colord
fi

if [ -d usr/share/help ]; then
  /usr/bin/rarian-sk-update 1> /dev/null 2> /dev/null
fi

if [ -d usr/share/applications ]; then
  /usr/bin/update-desktop-database -q usr/share/applications >/dev/null 2>&1
fi

if [ -d usr/share/mime ]; then
  /usr/bin/update-mime-database usr/share/mime >/dev/null 2>&1
fi

if [ -e usr/share/icons/hicolor/icon-theme.cache ]; then
  if [ -x /usr/bin/gtk-update-icon-cache ]; then
    /usr/bin/gtk-update-icon-cache usr/share/icons/hicolor >/dev/null 2>&1
  fi
fi

if [ -d usr/share/glib-2.0/schemas ]; then
  /usr/bin/glib-compile-schemas usr/share/glib-2.0/schemas >/dev/null 2>&1
fi
